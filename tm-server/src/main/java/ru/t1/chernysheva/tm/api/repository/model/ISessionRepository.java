package ru.t1.chernysheva.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import ru.t1.chernysheva.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {

    boolean existsById(@NotNull String id);

    void remove(@NotNull Session model);

}
