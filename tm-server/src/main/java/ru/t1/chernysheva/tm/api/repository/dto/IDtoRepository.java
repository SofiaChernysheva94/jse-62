package ru.t1.chernysheva.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.model.AbstractModelDTO;
import ru.t1.chernysheva.tm.enumerated.EntitySort;

import java.util.List;

public interface IDtoRepository<M extends AbstractModelDTO> {


    void add(@NotNull M model);

    void clear();

    boolean existsById(@NotNull String id);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@Nullable final EntitySort entitySort);

    @Nullable
    M findOneById(@NotNull String id);

    @Nullable
    M findOneByIndex(@NotNull Integer index);

    int getSize();

    void remove(@NotNull M model);

    void removeById(@NotNull String id);

    void update(@NotNull M model);

}
