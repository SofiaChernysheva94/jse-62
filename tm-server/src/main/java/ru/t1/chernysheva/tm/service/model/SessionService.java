package ru.t1.chernysheva.tm.service.model;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.chernysheva.tm.api.service.model.ISessionService;
import ru.t1.chernysheva.tm.exception.field.IdEmptyException;
import ru.t1.chernysheva.tm.exception.field.IndexIncorrectException;
import ru.t1.chernysheva.tm.exception.field.UserIdEmptyException;
import ru.t1.chernysheva.tm.exception.system.AuthenticationException;
import ru.t1.chernysheva.tm.model.Session;
import ru.t1.chernysheva.tm.repository.model.ISessionEntityRepository;

import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class SessionService implements ISessionService {

    @NotNull
    @Autowired
    private ISessionEntityRepository repository;

    @Override
    @Transactional
    public void clear(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        repository.deleteAllByUserId(userId);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Session> findAll(@Nullable final String userId) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        @NotNull List<Session> result;
        result = repository.findAllByUserId(userId);
        return result;
    }

    @Override
    @SneakyThrows
    @Transactional
    public boolean existsById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        boolean result;
        result = repository.existsById(id);
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Session findOneById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable Session result;
        result = repository.getOneByUserIdAndId(userId, id);
        return result;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Session findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        final Pageable pageable = PageRequest.of(index - 1, 1);
        @Nullable final List<Session> sessionList = repository.getOneByIndexAndUserId(userId, pageable);
        if (sessionList == null || sessionList.isEmpty()) return null;
        @Nullable Session result = sessionList.get(0);
        return result;

    }

    @Override
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteByUserIdAndId(userId, id);
    }

    @Override
    @Transactional
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null || index < 1) throw new IndexIncorrectException();
        final Pageable pageable = PageRequest.of(index - 1, 1);
        @Nullable final List<Session> sessionList = repository.getOneByIndexAndUserId(userId, pageable);
        if (sessionList == null || sessionList.isEmpty()) return;
        @Nullable Session session = sessionList.get(0);
        repository.deleteByUserIdAndId(userId, session.getId());

    }

    @Override
    @Transactional
    public void remove(@NotNull Session session) {
        if (session.getUser() == null) throw new UserIdEmptyException();
        repository.deleteByUserIdAndId(session.getUser().getId(), session.getId());
    }

    @Override
    @Transactional
    public void add(@Nullable final String userId, @Nullable final Session session) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (session == null) throw new AuthenticationException();
        repository.save(session);
    }

    @NotNull
    @Override
    @Transactional
    public Session add(@NotNull final Session session) {
        if (session.getUser() == null) throw new UserIdEmptyException();
        if (session.getUser().getId().isEmpty()) throw new UserIdEmptyException();
        repository.save(session);
        return session;
    }

}
