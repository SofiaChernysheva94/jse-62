package ru.t1.chernysheva.tm.repository.model;

import org.springframework.stereotype.Repository;
import ru.t1.chernysheva.tm.dto.model.SessionDTO;
import ru.t1.chernysheva.tm.model.Session;

@Repository
public interface ISessionEntityRepository extends IModelUserOwnedRepository<Session> {
}
