package ru.t1.chernysheva.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.data.repository.query.Param;
import ru.t1.chernysheva.tm.enumerated.EntitySort;
import ru.t1.chernysheva.tm.model.AbstractModel;

import java.util.List;

@NoRepositoryBean
public interface IModelUserOwnedRepository<M extends AbstractModel> extends JpaRepository<M, String> {


    void deleteAllByUserId(@NotNull String userId);

    void deleteAllByUserIdAndId(@NotNull String userId, @NotNull String id);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

    @NotNull
    List<M> findAllByUserId(@NotNull String userId);

    @NotNull
    @Query("SELECT e FROM #{#entityName} e WHERE e.user.id = :userId")
    List<M> findAllSortByUserId(@NotNull @Param("userId") String userId, @NotNull Sort entitySort);

    @Nullable
    @Query("SELECT e FROM #{#entityName} e WHERE e.user.id = :userId")
    List<M> getOneByIndexAndUserId(@NotNull @Param("userId") String userId, @NotNull Pageable pageable);

    @Nullable
    M getOneByUserIdAndId(@NotNull String userId, @NotNull String id);

    long countByUserId(@NotNull String userId);

    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

}
