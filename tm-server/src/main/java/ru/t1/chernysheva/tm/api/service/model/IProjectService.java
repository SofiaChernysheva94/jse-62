package ru.t1.chernysheva.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.enumerated.EntitySort;
import ru.t1.chernysheva.tm.enumerated.Status;
import ru.t1.chernysheva.tm.model.Project;

import java.util.List;

public interface IProjectService {

    void clear(@Nullable final String userId);

    @NotNull
    List<Project> findAll(@Nullable final String userId);

    boolean existsById(@Nullable final String userId, @Nullable final String id);

    @Nullable
    Project findOneById(@Nullable final String userId, @Nullable final String id);

    @Nullable
    Project findOneByIndex(@Nullable final String userId, @Nullable final Integer index);

    void removeById(@Nullable final String userId, @Nullable final String id);

    void removeByIndex(@Nullable final String userId, @Nullable final Integer index);

    @NotNull
    List<Project> findAll(@Nullable final String userId, @Nullable final EntitySort entitySort);

    @NotNull
    Project changeProjectStatusById(@Nullable String userId, @Nullable String id, @Nullable Status status);

    @NotNull
    Project changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status);

    void create(@Nullable String userId, @Nullable String name, @Nullable String description);

    void add(@Nullable String userId, @Nullable Project project);

    void updateById(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description);

    void updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description);

}
