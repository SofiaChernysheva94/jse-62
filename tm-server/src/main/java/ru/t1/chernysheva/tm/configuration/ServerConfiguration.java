package ru.t1.chernysheva.tm.configuration;

import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import ru.t1.chernysheva.tm.api.service.IDatabasePropertyService;
import ru.t1.chernysheva.tm.service.PropertyService;

import javax.sql.DataSource;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import java.util.Properties;


@Configuration
@ComponentScan("ru.t1.chernysheva.tm")
@EnableJpaRepositories("ru.t1.chernysheva.tm.repository")
@EnableTransactionManagement
public class ServerConfiguration {

    @NotNull
    @Autowired
    private PropertyService propertyService;

    @Bean
    @NotNull
    public DataSource dataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(propertyService.getDBDriver());
        dataSource.setUrl(propertyService.getDBUrl() + propertyService.getDBSchema());
        dataSource.setUsername(propertyService.getDBUser());
        dataSource.setPassword(propertyService.getDBPassword());
        return dataSource;
    }

    @Bean
    @NotNull
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(@NotNull final DataSource dataSource) {
        final LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.t1.chernysheva.tm.model", "ru.t1.chernysheva.tm.dto");
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.DIALECT, propertyService.getDBDialect());
        properties.put(Environment.FORMAT_SQL, "true");
        properties.put(Environment.SHOW_SQL, Boolean.parseBoolean(propertyService.getDBShowSQL()));
        properties.put(Environment.HBM2DDL_AUTO, propertyService.getDBHbm2DDL());
        if ("true".equals(propertyService.getDBL2Cache())) {
            properties.put(Environment.USE_SECOND_LEVEL_CACHE, propertyService.getDBL2Cache());
            properties.put(Environment.CACHE_REGION_FACTORY, propertyService.getDBCacheRegion());
            properties.put(Environment.USE_QUERY_CACHE, propertyService.getDBQueryCache());
            properties.put(Environment.USE_MINIMAL_PUTS, propertyService.getDBMinimalPuts());
            properties.put(Environment.CACHE_REGION_PREFIX, propertyService.getDBCacheRegionPrefix());
            properties.put(Environment.CACHE_PROVIDER_CONFIG, propertyService.getDBCacheProvider());
        }
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }



    @Bean
    @Scope("prototype")
    public EntityManager entityManager(@NotNull final EntityManagerFactory entityManagerFactory) {
        return entityManagerFactory.createEntityManager();
    }


}
