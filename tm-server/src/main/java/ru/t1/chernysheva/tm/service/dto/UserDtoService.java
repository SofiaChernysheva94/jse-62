package ru.t1.chernysheva.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.chernysheva.tm.api.service.IPropertyService;
import ru.t1.chernysheva.tm.api.service.dto.IUserDtoService;
import ru.t1.chernysheva.tm.dto.model.UserDTO;
import ru.t1.chernysheva.tm.enumerated.Role;
import ru.t1.chernysheva.tm.exception.entity.EntityNotFoundException;
import ru.t1.chernysheva.tm.exception.entity.UserNotFoundException;
import ru.t1.chernysheva.tm.exception.field.*;
import ru.t1.chernysheva.tm.repository.dto.IProjectDtoEntityRepository;
import ru.t1.chernysheva.tm.repository.dto.ITaskDtoEntityRepository;
import ru.t1.chernysheva.tm.repository.dto.IUserDtoEntityRepository;
import ru.t1.chernysheva.tm.util.HashUtil;

import java.util.List;

@Service
@NoArgsConstructor
@AllArgsConstructor
public class UserDtoService implements IUserDtoService {

    @NotNull
    @Autowired
    private IUserDtoEntityRepository repository;

    @NotNull
    @Autowired
    private ITaskDtoEntityRepository taskRepository;

    @NotNull
    @Autowired
    private IProjectDtoEntityRepository projectRepository;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @SneakyThrows
    @Override
    @Transactional
    public UserDTO create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExist(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        repository.save(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull UserDTO user = new UserDTO();
        if (repository.existsByLogin(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        if (email != null && !email.isEmpty()) {
            if (repository.existsByEmail(email)) throw new ExistsEmailException();
            user.setEmail(email);
        }
        repository.save(user);
        return user;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO create(@Nullable final String login, @Nullable final String password, @Nullable final Role role) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @NotNull UserDTO user = new UserDTO();
        if (repository.existsByLogin(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        repository.save(user);
        return user;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void clear() {
        repository.deleteAll();
    }

    @Override
    @SneakyThrows
    @Transactional
    public void add(@Nullable UserDTO user) {
        if (user == null) throw new EntityNotFoundException();
        repository.save(user);
    }

    @Override
    @SneakyThrows
    public @NotNull List<UserDTO> findAll() {
        return repository.findAll();
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findById(@Nullable final String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        return repository.findById(id).orElse(null);
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.findByEmail(email);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable UserDTO user;
        user = repository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        taskRepository.deleteAllByUserId(user.getId());
        projectRepository.deleteAllByUserId(user.getId());
        repository.delete(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable UserDTO user;
        user = repository.findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        taskRepository.deleteAllByUserId(user.getId());
        projectRepository.deleteAllByUserId(user.getId());
        repository.delete(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void remove(@Nullable final UserDTO user) {
        if (user == null) throw new UserNotFoundException();
        @NotNull final String userId = user.getId();
        taskRepository.deleteAllByUserId(userId);
        projectRepository.deleteAllByUserId(userId);
        repository.delete(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@Nullable String id) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        repository.deleteById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable UserDTO user;
        user = repository.findById(id).orElse(null);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        repository.save(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable UserDTO user;
        user = repository.findById(id).orElse(null);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        repository.save(user);
    }

    @Override
    @SneakyThrows
    public Boolean isLoginExist(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        boolean result;
        result = repository.existsByLogin(login);
        return result;
    }

    @Override
    @SneakyThrows
    public Boolean isEmailExist(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        boolean result;
        result = repository.existsByEmail(email);
        return result;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable UserDTO user;
        user = repository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        repository.save(user);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable UserDTO user;
        user = repository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        repository.save(user);
    }

}
