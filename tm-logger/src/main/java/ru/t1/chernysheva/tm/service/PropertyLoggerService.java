package ru.t1.chernysheva.tm.service;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;

@Getter
@Service
@PropertySource("classpath:application.properties")
public final class PropertyLoggerService {

    @Value("#{environment['jms.host']}")
    public String jmsHost;

    @Value("#{environment['jms.port']}")
    public String jmsPort;

    @Value("#{environment['jms.database']}")
    private String jmsDatabase;

    @Value("#{environment['jms.url']}")
    private String jmsBrokerUrl;

}
