package ru.t1.chernysheva.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.enumerated.EntitySort;

@Getter
@Setter
@NoArgsConstructor
public final class ProjectListRequest extends AbstractUserRequest {

    @Nullable
    private EntitySort entitySort;

    public ProjectListRequest(@Nullable final String token) {
        super(token);
    }

}
