package ru.t1.chernysheva.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.dto.model.UserDTO;

@NoArgsConstructor
public final class UserLogoutResponse extends AbstractUserResponse {

    public UserLogoutResponse(@Nullable final UserDTO user) {
        super(user);
    }

}
