package ru.t1.chernysheva.tm.enumerated;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.chernysheva.tm.comparator.CreatedComparator;
import ru.t1.chernysheva.tm.comparator.NameComparator;
import ru.t1.chernysheva.tm.comparator.StatusComparator;

import java.util.Comparator;

public enum EntitySort {

    BY_NAME("Sort by name", "name", NameComparator.INSTANCE),
    BY_STATUS("Sort by status", "status", StatusComparator.INSTANCE),
    BY_CREATED("Sort by created", "created", CreatedComparator.INSTANCE);

    @NotNull
    private final String displayName;

    @NotNull
    private final String sortField;

    @NotNull
    private final Comparator<?> comparator;

    @Nullable
    public static EntitySort toSort(@Nullable final String value) {
        if (value == null || value.isEmpty()) return null;
        for (@NotNull final EntitySort entitySort : values()) {
            if (entitySort.name().equals(value)) return entitySort;
        }
        return null;
    }

    EntitySort(@NotNull final String displayName, @NotNull final String sortField, @NotNull final Comparator<?> comparator) {
        this.displayName = displayName;
        this.sortField = sortField;
        this.comparator = comparator;
    }

    @NotNull
    public String getDisplayName() {
        return displayName;
    }

    @NotNull
    public String getSortField() {
        return sortField;
    }

    @NotNull
    public Comparator getComparator() {
        return comparator;
    }

}
