package ru.t1.chernysheva.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.chernysheva.tm.event.ConsoleEvent;

@Component
public final class ApplicationExitListener extends AbstractSystemListener {

    @NotNull
    public static final String DESCRIPTION = "Close application.";

    @NotNull
    public static final String NAME = "exit";

    @Nullable
    @Override
    public String getArgument() {
        return null;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@applicationExitListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.exit(0);
    }

}
