package ru.t1.chernysheva.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.chernysheva.tm.dto.model.TaskDTO;
import ru.t1.chernysheva.tm.dto.request.TaskListRequest;
import ru.t1.chernysheva.tm.enumerated.EntitySort;
import ru.t1.chernysheva.tm.event.ConsoleEvent;
import ru.t1.chernysheva.tm.util.TerminalUtil;

import java.util.Arrays;
import java.util.List;

@Component
public final class TaskListListener extends AbstractTaskListener {

    @NotNull
    public static final String DESCRIPTION = "Show all tasks.";

    @NotNull
    public static final String NAME = "task-list";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@taskListListener.getName() == #consoleEvent.name")
    public void execute(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[TASK LIST]");
        System.out.println("ENTER SORT:");
        System.out.println(Arrays.toString(EntitySort.values()));
        @NotNull final String sortType = TerminalUtil.nextLine();
        @Nullable final EntitySort entitySort = EntitySort.toSort(sortType);

        @NotNull final TaskListRequest request = new TaskListRequest(getToken());
        request.setEntitySort(entitySort);

        @Nullable final List<TaskDTO> tasks = taskEndpoint.listTask(request).getTasks();
        if (tasks != null) renderTasks(tasks);
    }

}
